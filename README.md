# mqtt-thingspeak

Example of using mqtt, paho-mqtt, Thingspeak and a RPi

This repo has 3 examples

* `publish.py` publish to a thingspeak channel
* `subscribe.py` subscribe to a thingspeak channel
* `publish_subscribe.py` publish and subscribe in one file 

When connecting to Thingspeak, ensure that you have set up seperate MQTT devices for each connection at thingspeak.com

## References

Inspiration from http://www.steves-internet-guide.com/into-mqtt-python-client/  
created by: Nikolaj Simonsen | November 2021  
repo: https://gitlab.com/npes-py-experiments/mqtt-thingspeak  

paho-mqtt lib reference: https://pypi.org/project/paho-mqtt/  
thingspeak MQTT setup: https://se.mathworks.com/help/thingspeak/mqtt-basics.html

## setup instructions

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/mqtt-tests.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements from requirements.txt https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#using-requirements-files
4. Run `python3 <appname.py>` (linux) or `py <appname.py>` (windows)
