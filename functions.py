import sys

# open and format thingspeak MQTT credential file, return as a dictionary
def get_tokens(filename='token.txt', out=sys.stdout):
    out.write('using token path {}\n'.format(filename))
    tokens = dict()
    with open(filename) as f:
        for line in f:
            words = line.split(' = ')
            if words[0] not in tokens:
                tokens[words[0].strip()] = words[1].strip()
    out.write(f'- read token file with {len(tokens)} lines\n')

    return tokens
